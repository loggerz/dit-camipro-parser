use std::collections::HashMap;
use serde::Deserialize;

#[derive(Debug, Deserialize, Eq, PartialEq)]
pub struct CamiproCsv {
    pub sciper: String,
    pub card_id: String,
    pub card_status: String,
    pub status: String,
    pub issue_date: String,
    pub card_status_date: String,
    pub card_id_old: String,
    pub library_id: String,
}

pub fn load_camipro_records(path: &str) -> Result<Vec<CamiproCsv>, String> {
    let reader = csv::Reader::from_path(path);

    let records: Result<Vec<CamiproCsv>, _> =
        reader
            .map_err(|e| e.to_string())?
            .deserialize()
            .collect();

    Ok(records.map_err(|e| e.to_string())?)
}

pub fn camipro_vec_to_map(camipros: Vec<CamiproCsv>) -> HashMap<String, CamiproCsv> {
    let mut camipros_map: HashMap<String, CamiproCsv> = HashMap::new();
    for camipro in camipros {
        camipros_map.insert(camipro.sciper.clone(), camipro);
    }
    camipros_map
}