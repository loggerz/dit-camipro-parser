use epfl_people::models::IndividualWithUnitPaths;
use std::fs;

use crate::camipro::{camipro_vec_to_map, load_camipro_records};
use crate::lib::*;

mod camipro;
mod lib;

#[tokio::main]
async fn main() {
    let mx_people = get_people_from_units(vec!["MX-S".to_owned()]).await;
    let camipros = camipro_vec_to_map(load_camipro_records("./camipro 12 dec 22.csv").unwrap());

    let mut missing_camipro_record: Vec<IndividualWithUnitPaths> = vec!();
    let mut card_ids_output = String::new();

    for people in mx_people {
        if let Some(camipro) = camipros.get(&people.sciper) {
            let str: String = format!(r##"  "{}",{}"##, camipro.card_id, "\n");
            card_ids_output.push_str(&str);
        } else {
            missing_camipro_record.push(people);
        }
    }

    if missing_camipro_record.len() > 0 {
        println!("\n---\nCould not find camipro records for these people : ");
        for p in missing_camipro_record {
            println!("  -  ({}) {}", p.sciper, p.name);
        }
    }

    fs::write("card_ids.txt", card_ids_output).expect("Failed to write to card_ids.txt");

    println!("\n---\nCamipros card ids dump written in file card_ids.txt\n");
}
