use epfl_people::{build_people_map, load_recursive_units, save_people_map};
use epfl_people::models::IndividualWithUnitPaths;

pub async fn get_people_from_units(units: Vec<String>) -> Vec<IndividualWithUnitPaths> {
    let root_unit = load_recursive_units().await.unwrap();
    let people_map = build_people_map(root_unit);
    save_people_map(people_map.clone()).await;

    let mut selected_individuals: Vec<IndividualWithUnitPaths> = vec!();

    for (_, people) in people_map {
        for unit_path in &people.unit_paths {
            for unit in &units {
                if unit_path.contains(unit) {
                    selected_individuals.push(people.clone());
                }
            }
        }
    }

    selected_individuals
}